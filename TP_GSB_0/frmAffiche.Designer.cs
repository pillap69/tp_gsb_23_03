﻿namespace TP_GSB_0
{
    partial class frmAffiche
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAffiche = new System.Windows.Forms.Button();
            this.tbAffiche = new System.Windows.Forms.TextBox();
            this.btnAffiche_2 = new System.Windows.Forms.Button();
            this.tbID = new System.Windows.Forms.TextBox();
            this.lbID = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAffiche
            // 
            this.btnAffiche.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAffiche.Location = new System.Drawing.Point(266, 264);
            this.btnAffiche.Name = "btnAffiche";
            this.btnAffiche.Size = new System.Drawing.Size(136, 45);
            this.btnAffiche.TabIndex = 0;
            this.btnAffiche.Text = "Affiche";
            this.btnAffiche.UseVisualStyleBackColor = true;
            this.btnAffiche.Click += new System.EventHandler(this.btnAffiche_Click);
            // 
            // tbAffiche
            // 
            this.tbAffiche.Location = new System.Drawing.Point(61, 80);
            this.tbAffiche.Multiline = true;
            this.tbAffiche.Name = "tbAffiche";
            this.tbAffiche.Size = new System.Drawing.Size(423, 153);
            this.tbAffiche.TabIndex = 1;
            // 
            // btnAffiche_2
            // 
            this.btnAffiche_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAffiche_2.Location = new System.Drawing.Point(61, 264);
            this.btnAffiche_2.Name = "btnAffiche_2";
            this.btnAffiche_2.Size = new System.Drawing.Size(199, 44);
            this.btnAffiche_2.TabIndex = 2;
            this.btnAffiche_2.Text = "Affiche un visiteur";
            this.btnAffiche_2.UseVisualStyleBackColor = true;
            this.btnAffiche_2.Click += new System.EventHandler(this.btnAffiche_2_Click);
            // 
            // tbID
            // 
            this.tbID.Location = new System.Drawing.Point(201, 30);
            this.tbID.Name = "tbID";
            this.tbID.Size = new System.Drawing.Size(173, 20);
            this.tbID.TabIndex = 3;
            // 
            // lbID
            // 
            this.lbID.AutoSize = true;
            this.lbID.Location = new System.Drawing.Point(58, 33);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(128, 13);
            this.lbID.TabIndex = 4;
            this.lbID.Text = "SAISIR UN ID VIISTEUR";
            // 
            // frmAffiche
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 344);
            this.Controls.Add(this.lbID);
            this.Controls.Add(this.tbID);
            this.Controls.Add(this.btnAffiche_2);
            this.Controls.Add(this.tbAffiche);
            this.Controls.Add(this.btnAffiche);
            this.Name = "frmAffiche";
            this.Text = "Affiche Visiteurs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAffiche;
        private System.Windows.Forms.TextBox tbAffiche;
        private System.Windows.Forms.Button btnAffiche_2;
        private System.Windows.Forms.TextBox tbID;
        private System.Windows.Forms.Label lbID;
    }
}

