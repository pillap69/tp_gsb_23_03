﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_gsb.mesClasses.outils;
using MySql.Data.MySqlClient;

namespace TP_gsb
{
    public partial class frmSaisieVisiteur : Form
    {
        public frmSaisieVisiteur()
        {
            InitializeComponent();
           
        }

        

        private bool verifSaisie()
        {
            foreach (Control octrl in this.Controls)
            {
                if(octrl is TextBox || octrl is MaskedTextBox)
                {
                    if(octrl.Text == string.Empty)
                    {
                        return false;
                    }
                }
            }

            return true;

        }

        private void reinitTb()
        {
            foreach (Control octrl in this.Controls)
            {
                if (octrl is TextBox)
                {
                    TextBox otb = (TextBox)octrl; //cast dans le bon type
                    otb.Text = string.Empty;
                }
                else if (octrl is MaskedTextBox) {
                    MaskedTextBox otb = (MaskedTextBox)octrl; //cast dans le bon type
                    otb.Text = string.Empty;

                }

            }


        }

        private string faireLogin(string sprenom, string snom)
        {
            string lettre1Prenom = sprenom.Substring(0, 1);
            string login = lettre1Prenom + snom;
            return login;

        }

        private void btnValidation_Click(object sender, EventArgs e)
        {
            bool etat = verifSaisie();
            if(etat)
            {
                Cdao odao = new Cdao();
                string leLogin = faireLogin(tbprenom.Text, tbnom.Text);
                DateTime odateEmbauche = Convert.ToDateTime(dtpEmbauche.Value);
                string dateEmbaucheMySql = CtraitementDate.getDateFormatMysql(odateEmbauche);
                //values('toto','titi')
                string query = "insert into visiteur(id,nom,prenom,login,mdp,adresse,cp,ville,dateEmbauche) values('" + tbid.Text.Trim() + "','" + tbnom.Text.Trim() + "','" +
                    tbprenom.Text.Trim() + "','" + leLogin.Trim() + "','" + tbmdp.Text.Trim() + "','" + tbadresse.Text.Trim() + "','" + mtbcp.Text.Trim() + "','" +
                    tbville.Text.Trim() + "','" + dateEmbaucheMySql + "')";

                try //remontée d'exception
                {
                    odao.insertEnreg(query);

                    reinitTb();
                    DialogResult result = MessageBox.Show("Enregistrement bien effectué, voulez-vous effectuer une autre saisie ? ","Avertissement",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                    if(result == DialogResult.Yes) // DialogResult est une énumération
                    {
                        reinitTb();
                    }
                    else
                    {
                        this.Close();
                    }

                }
                catch(MySqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            else
            {
                MessageBox.Show("Vérifier la saisie des champs !", "Attention !");
            }
        }

        private void afficheMdp(object sender, EventArgs e)
        {
            if (chkboxmdp.Checked)
            {
                tbmdp.PasswordChar = '\0';
            }
            else
            {
                tbmdp.PasswordChar = '*';
            }
        }

        private void btnannuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
