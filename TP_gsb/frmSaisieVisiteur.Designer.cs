﻿namespace TP_gsb
{
    partial class frmSaisieVisiteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbnom = new System.Windows.Forms.TextBox();
            this.tbprenom = new System.Windows.Forms.TextBox();
            this.tbmdp = new System.Windows.Forms.TextBox();
            this.tbadresse = new System.Windows.Forms.TextBox();
            this.lbnom = new System.Windows.Forms.Label();
            this.lbprenom = new System.Windows.Forms.Label();
            this.lbmdp = new System.Windows.Forms.Label();
            this.lbadresse = new System.Windows.Forms.Label();
            this.lbcp = new System.Windows.Forms.Label();
            this.lbdateembauche = new System.Windows.Forms.Label();
            this.btnValidation = new System.Windows.Forms.Button();
            this.tbville = new System.Windows.Forms.TextBox();
            this.lbville = new System.Windows.Forms.Label();
            this.lbid = new System.Windows.Forms.Label();
            this.tbid = new System.Windows.Forms.TextBox();
            this.mtbcp = new System.Windows.Forms.MaskedTextBox();
            this.chkboxmdp = new System.Windows.Forms.CheckBox();
            this.btnannuler = new System.Windows.Forms.Button();
            this.dtpEmbauche = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // tbnom
            // 
            this.tbnom.Location = new System.Drawing.Point(159, 103);
            this.tbnom.Name = "tbnom";
            this.tbnom.Size = new System.Drawing.Size(172, 22);
            this.tbnom.TabIndex = 1;
            // 
            // tbprenom
            // 
            this.tbprenom.Location = new System.Drawing.Point(159, 157);
            this.tbprenom.Name = "tbprenom";
            this.tbprenom.Size = new System.Drawing.Size(172, 22);
            this.tbprenom.TabIndex = 2;
            // 
            // tbmdp
            // 
            this.tbmdp.Location = new System.Drawing.Point(159, 203);
            this.tbmdp.Name = "tbmdp";
            this.tbmdp.PasswordChar = '*';
            this.tbmdp.Size = new System.Drawing.Size(170, 22);
            this.tbmdp.TabIndex = 3;
            // 
            // tbadresse
            // 
            this.tbadresse.Location = new System.Drawing.Point(159, 267);
            this.tbadresse.Multiline = true;
            this.tbadresse.Name = "tbadresse";
            this.tbadresse.Size = new System.Drawing.Size(209, 53);
            this.tbadresse.TabIndex = 5;
            // 
            // lbnom
            // 
            this.lbnom.AutoSize = true;
            this.lbnom.Location = new System.Drawing.Point(24, 103);
            this.lbnom.Name = "lbnom";
            this.lbnom.Size = new System.Drawing.Size(45, 17);
            this.lbnom.TabIndex = 6;
            this.lbnom.Text = "Nom :";
            // 
            // lbprenom
            // 
            this.lbprenom.AutoSize = true;
            this.lbprenom.Location = new System.Drawing.Point(24, 157);
            this.lbprenom.Name = "lbprenom";
            this.lbprenom.Size = new System.Drawing.Size(65, 17);
            this.lbprenom.TabIndex = 7;
            this.lbprenom.Text = "Prénom :";
            // 
            // lbmdp
            // 
            this.lbmdp.AutoSize = true;
            this.lbmdp.Location = new System.Drawing.Point(26, 206);
            this.lbmdp.Name = "lbmdp";
            this.lbmdp.Size = new System.Drawing.Size(101, 17);
            this.lbmdp.TabIndex = 8;
            this.lbmdp.Text = "Mot de passe :";
            // 
            // lbadresse
            // 
            this.lbadresse.AutoSize = true;
            this.lbadresse.Location = new System.Drawing.Point(24, 270);
            this.lbadresse.Name = "lbadresse";
            this.lbadresse.Size = new System.Drawing.Size(68, 17);
            this.lbadresse.TabIndex = 9;
            this.lbadresse.Text = "Adresse :";
            // 
            // lbcp
            // 
            this.lbcp.AutoSize = true;
            this.lbcp.Location = new System.Drawing.Point(24, 346);
            this.lbcp.Name = "lbcp";
            this.lbcp.Size = new System.Drawing.Size(91, 17);
            this.lbcp.TabIndex = 10;
            this.lbcp.Text = "Code postal :";
            // 
            // lbdateembauche
            // 
            this.lbdateembauche.AutoSize = true;
            this.lbdateembauche.Location = new System.Drawing.Point(24, 439);
            this.lbdateembauche.Name = "lbdateembauche";
            this.lbdateembauche.Size = new System.Drawing.Size(116, 17);
            this.lbdateembauche.TabIndex = 11;
            this.lbdateembauche.Text = "Date embauche :";
            // 
            // btnValidation
            // 
            this.btnValidation.Location = new System.Drawing.Point(156, 489);
            this.btnValidation.Name = "btnValidation";
            this.btnValidation.Size = new System.Drawing.Size(103, 32);
            this.btnValidation.TabIndex = 10;
            this.btnValidation.Text = "Valider Saisie";
            this.btnValidation.UseVisualStyleBackColor = true;
            this.btnValidation.Click += new System.EventHandler(this.btnValidation_Click);
            // 
            // tbville
            // 
            this.tbville.Location = new System.Drawing.Point(156, 388);
            this.tbville.Name = "tbville";
            this.tbville.Size = new System.Drawing.Size(170, 22);
            this.tbville.TabIndex = 7;
            // 
            // lbville
            // 
            this.lbville.AutoSize = true;
            this.lbville.Location = new System.Drawing.Point(24, 391);
            this.lbville.Name = "lbville";
            this.lbville.Size = new System.Drawing.Size(42, 17);
            this.lbville.TabIndex = 14;
            this.lbville.Text = "Ville :";
            // 
            // lbid
            // 
            this.lbid.AutoSize = true;
            this.lbid.Location = new System.Drawing.Point(24, 45);
            this.lbid.Name = "lbid";
            this.lbid.Size = new System.Drawing.Size(81, 17);
            this.lbid.TabIndex = 16;
            this.lbid.Text = "Identidiant :";
            // 
            // tbid
            // 
            this.tbid.Location = new System.Drawing.Point(156, 43);
            this.tbid.Name = "tbid";
            this.tbid.Size = new System.Drawing.Size(172, 22);
            this.tbid.TabIndex = 0;
            // 
            // mtbcp
            // 
            this.mtbcp.Location = new System.Drawing.Point(156, 343);
            this.mtbcp.Mask = "00000";
            this.mtbcp.Name = "mtbcp";
            this.mtbcp.Size = new System.Drawing.Size(68, 22);
            this.mtbcp.TabIndex = 6;
            // 
            // chkboxmdp
            // 
            this.chkboxmdp.AutoSize = true;
            this.chkboxmdp.Location = new System.Drawing.Point(175, 231);
            this.chkboxmdp.Name = "chkboxmdp";
            this.chkboxmdp.Size = new System.Drawing.Size(142, 21);
            this.chkboxmdp.TabIndex = 4;
            this.chkboxmdp.Text = "voir mot de passe";
            this.chkboxmdp.UseVisualStyleBackColor = true;
            this.chkboxmdp.CheckedChanged += new System.EventHandler(this.afficheMdp);
            // 
            // btnannuler
            // 
            this.btnannuler.Location = new System.Drawing.Point(279, 489);
            this.btnannuler.Name = "btnannuler";
            this.btnannuler.Size = new System.Drawing.Size(105, 30);
            this.btnannuler.TabIndex = 9;
            this.btnannuler.Text = "Annuler";
            this.btnannuler.UseVisualStyleBackColor = true;
            this.btnannuler.Click += new System.EventHandler(this.btnannuler_Click);
            // 
            // dtpEmbauche
            // 
            this.dtpEmbauche.Location = new System.Drawing.Point(156, 434);
            this.dtpEmbauche.Name = "dtpEmbauche";
            this.dtpEmbauche.Size = new System.Drawing.Size(228, 22);
            this.dtpEmbauche.TabIndex = 17;
            // 
            // frmSaisieVisiteur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 533);
            this.Controls.Add(this.dtpEmbauche);
            this.Controls.Add(this.btnannuler);
            this.Controls.Add(this.chkboxmdp);
            this.Controls.Add(this.mtbcp);
            this.Controls.Add(this.tbid);
            this.Controls.Add(this.lbid);
            this.Controls.Add(this.lbville);
            this.Controls.Add(this.tbville);
            this.Controls.Add(this.btnValidation);
            this.Controls.Add(this.lbdateembauche);
            this.Controls.Add(this.lbcp);
            this.Controls.Add(this.lbadresse);
            this.Controls.Add(this.lbmdp);
            this.Controls.Add(this.lbprenom);
            this.Controls.Add(this.lbnom);
            this.Controls.Add(this.tbadresse);
            this.Controls.Add(this.tbmdp);
            this.Controls.Add(this.tbprenom);
            this.Controls.Add(this.tbnom);
            this.MaximumSize = new System.Drawing.Size(431, 580);
            this.Name = "frmSaisieVisiteur";
            this.Text = "Saisie des nouveaux visiteurs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbnom;
        private System.Windows.Forms.TextBox tbprenom;
        private System.Windows.Forms.TextBox tbmdp;
        private System.Windows.Forms.TextBox tbadresse;
        private System.Windows.Forms.Label lbnom;
        private System.Windows.Forms.Label lbprenom;
        private System.Windows.Forms.Label lbmdp;
        private System.Windows.Forms.Label lbadresse;
        private System.Windows.Forms.Label lbcp;
        private System.Windows.Forms.Label lbdateembauche;
        private System.Windows.Forms.Button btnValidation;
        private System.Windows.Forms.TextBox tbville;
        private System.Windows.Forms.Label lbville;
        private System.Windows.Forms.Label lbid;
        private System.Windows.Forms.TextBox tbid;
        private System.Windows.Forms.MaskedTextBox mtbcp;
        private System.Windows.Forms.CheckBox chkboxmdp;
        private System.Windows.Forms.Button btnannuler;
        private System.Windows.Forms.DateTimePicker dtpEmbauche;
    }
}