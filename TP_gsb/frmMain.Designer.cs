﻿namespace TP_gsb
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.visiteurMédicauxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saisieDesFichesDeFraisMensuellesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.comptableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validationDesFichesDeFraisVisiteursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saisirNouveauVisiteurMalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAuthentification = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMdp = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visiteurMédicauxToolStripMenuItem,
            this.comptableToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(382, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // visiteurMédicauxToolStripMenuItem
            // 
            this.visiteurMédicauxToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisieDesFichesDeFraisMensuellesToolStripMenuItem,
            this.quitterToolStripMenuItem1});
            this.visiteurMédicauxToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.visiteurMédicauxToolStripMenuItem.Name = "visiteurMédicauxToolStripMenuItem";
            this.visiteurMédicauxToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.visiteurMédicauxToolStripMenuItem.Text = "Visiteurs médicaux";
            // 
            // saisieDesFichesDeFraisMensuellesToolStripMenuItem
            // 
            this.saisieDesFichesDeFraisMensuellesToolStripMenuItem.Name = "saisieDesFichesDeFraisMensuellesToolStripMenuItem";
            this.saisieDesFichesDeFraisMensuellesToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.saisieDesFichesDeFraisMensuellesToolStripMenuItem.Text = "Saisie des fiches de frais mensuelles";
            this.saisieDesFichesDeFraisMensuellesToolStripMenuItem.Click += new System.EventHandler(this.saisieDesFichesDeFraisMensuellesToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem1
            // 
            this.quitterToolStripMenuItem1.Name = "quitterToolStripMenuItem1";
            this.quitterToolStripMenuItem1.Size = new System.Drawing.Size(261, 22);
            this.quitterToolStripMenuItem1.Text = "Quitter";
            this.quitterToolStripMenuItem1.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // comptableToolStripMenuItem
            // 
            this.comptableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.validationDesFichesDeFraisVisiteursToolStripMenuItem,
            this.quitterToolStripMenuItem2});
            this.comptableToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.comptableToolStripMenuItem.Name = "comptableToolStripMenuItem";
            this.comptableToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.comptableToolStripMenuItem.Text = "Comptable";
            // 
            // validationDesFichesDeFraisVisiteursToolStripMenuItem
            // 
            this.validationDesFichesDeFraisVisiteursToolStripMenuItem.Name = "validationDesFichesDeFraisVisiteursToolStripMenuItem";
            this.validationDesFichesDeFraisVisiteursToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.validationDesFichesDeFraisVisiteursToolStripMenuItem.Text = "Validation des fiches de frais visiteurs";
            this.validationDesFichesDeFraisVisiteursToolStripMenuItem.Click += new System.EventHandler(this.validationDesFichesDeFraisVisiteursToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem2
            // 
            this.quitterToolStripMenuItem2.Name = "quitterToolStripMenuItem2";
            this.quitterToolStripMenuItem2.Size = new System.Drawing.Size(268, 22);
            this.quitterToolStripMenuItem2.Text = "Quitter";
            this.quitterToolStripMenuItem2.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisirNouveauVisiteurMalToolStripMenuItem,
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(98, 20);
            this.toolStripMenuItem1.Text = "Administration";
            // 
            // saisirNouveauVisiteurMalToolStripMenuItem
            // 
            this.saisirNouveauVisiteurMalToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.saisirNouveauVisiteurMalToolStripMenuItem.Name = "saisirNouveauVisiteurMalToolStripMenuItem";
            this.saisirNouveauVisiteurMalToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.saisirNouveauVisiteurMalToolStripMenuItem.Text = "Saisir Nouveau Visiteur Médical";
            this.saisirNouveauVisiteurMalToolStripMenuItem.Click += new System.EventHandler(this.saisirNouveauVisiteurMalToolStripMenuItem_Click);
            // 
            // listeDesVisiteursDeLaSociétéToolStripMenuItem
            // 
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Name = "listeDesVisiteursDeLaSociétéToolStripMenuItem";
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Text = "Liste des visiteurs de la société ";
            this.listeDesVisiteursDeLaSociétéToolStripMenuItem.Click += new System.EventHandler(this.listeDesVisiteursDeLaSociétéToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.quitterToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // btnAuthentification
            // 
            this.btnAuthentification.Location = new System.Drawing.Point(203, 174);
            this.btnAuthentification.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAuthentification.Name = "btnAuthentification";
            this.btnAuthentification.Size = new System.Drawing.Size(112, 33);
            this.btnAuthentification.TabIndex = 1;
            this.btnAuthentification.Text = "Connexion";
            this.btnAuthentification.UseVisualStyleBackColor = true;
            this.btnAuthentification.Click += new System.EventHandler(this.btnAuthentification_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 102);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "NOM UTILISATEUR :";
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(203, 98);
            this.tbLogin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(140, 20);
            this.tbLogin.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 133);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "MOT DE PASSE :";
            // 
            // tbMdp
            // 
            this.tbMdp.Location = new System.Drawing.Point(203, 129);
            this.tbMdp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMdp.Name = "tbMdp";
            this.tbMdp.PasswordChar = '*';
            this.tbMdp.Size = new System.Drawing.Size(138, 20);
            this.tbMdp.TabIndex = 5;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 317);
            this.Controls.Add(this.tbMdp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbLogin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAuthentification);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmMain";
            this.Text = "GSB";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saisirNouveauVisiteurMalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visiteurMédicauxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comptableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem listeDesVisiteursDeLaSociétéToolStripMenuItem;
        private System.Windows.Forms.Button btnAuthentification;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbMdp;
        private System.Windows.Forms.ToolStripMenuItem saisieDesFichesDeFraisMensuellesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validationDesFichesDeFraisVisiteursToolStripMenuItem;
    }
}

