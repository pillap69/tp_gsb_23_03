﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using TP_gsb.mesClasses.outils;
using TP_gsb.mesClasses;
using System.Data; //Dataset

namespace TP_gsb.mesClasses
{

    #region employe : Classe métier
    public static class CemployeAuthentifie
    {
        public static Cemploye oemplAuthentifie;

    }
    public class Cemploye
    {
        
        public string id_employe { get; set; }
        public string nom_employe { get; set; }
        public string prenom_employe { get; set; }

        public string login_employe { get; set; }
        public string mdp_employe { get; set; }
        public string adresse_employe { get; set; }
        public int cp_employe { get; set; }
        public string ville_employe { get; set; }
        public DateTime dateEmbauche_employe { get; set; }

        public bool estAuthentifie { get; set; }
        

        public Cemploye(string sid_employe, string snom_employe, string sprenom_employe, string slogin_employe, string smdp_employe, string sadresse_employe, int scp_employe, string sville_employe, DateTime sdateEmbauche_employe)
        {
            id_employe = sid_employe;
            nom_employe = snom_employe;
            prenom_employe = sprenom_employe;
            login_employe = slogin_employe;
            mdp_employe = smdp_employe;
            adresse_employe = sadresse_employe;
            cp_employe = scp_employe;
            ville_employe = sville_employe;
            dateEmbauche_employe = sdateEmbauche_employe;
            estAuthentifie = false;
        }

        

    }

    #endregion

    #region Visiteur : métier et contrôle
    public class Cvisiteur : Cemploye
    {

        public Cvisiteur(string sid_visiteur, string snom_visiteur, string sprenom_visiteur, string slogin_visiteur, string smdp_visiteur, string sadresse_visiteur, int scp_visiteur, string sville_visiteur, DateTime sdateEmbauche_visiteur) 
            : base(sid_visiteur, snom_visiteur, sprenom_visiteur, slogin_visiteur, smdp_visiteur, sadresse_visiteur, scp_visiteur, sville_visiteur, sdateEmbauche_visiteur)
        {
         
        }
        
    }

    public class Cvisiteurs
    {
        Cvisiteur[] tabVisit = new Cvisiteur[27];
        public Dictionary<string, Cvisiteur> ocollDicovisit;
        public Dictionary<string, Cvisiteur> ocollDicoVisitByLogin;
        public Cvisiteur ovisiteurAuthentifie = null;
        

        public Cvisiteurs()
        {
            ocollDicovisit = new Dictionary<string, Cvisiteur>();
            ocollDicoVisitByLogin = new Dictionary<string, Cvisiteur>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM visiteur";
            MySqlDataReader ord = odao.getReader(query);
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
            while (ord.Read())
            {
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]), Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                ocollDicovisit.Add(Convert.ToString(ord["id"]), ovisiteur);
                ocollDicoVisitByLogin.Add(Convert.ToString(ord["login"]), ovisiteur);
            }

        }

        public Cvisiteur getVisiteur(string skey) //la clef reçue est soit le "login" soit "id"
        {
            // Cette methode s'adapte au deux dictionnaires
            int valeur;
            Cvisiteur ovisiteur;
            bool trouve = false;
            
            //dans les id visiteur le deuxième caractère est un entier
            if (int.TryParse(skey.Substring(1,1), out valeur)) // permet de verifier que le deuxieme caractere de la clef est un int
            {
                trouve = ocollDicovisit.TryGetValue(skey, out ovisiteur);
            }
            else
            {
                trouve = ocollDicoVisitByLogin.TryGetValue(skey, out ovisiteur);

            }
           
            if(trouve)
            {
                return ovisiteur;
            }
            else
            {
                return null;
            }

        }

        public DataSet getdsVisiteur()
        {
            Cdao odao = new Cdao();
            string query = "select * from visiteur";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        private  static Cvisiteurs Instance = null;
        public  static Cvisiteurs getInstance()
        {
            if(Instance == null)
            {
                Instance = new Cvisiteurs();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }
    }

    #endregion

    #region Comptable : Classes métier et contôle

    public class Ccomptable : Cemploye
    {
       
        public Ccomptable(string sid_comptable, string snom_comptable, string sprenom_comptable, string slogin_comptable, string smdp_comptable, string sadresse_comptable, int scp_comptable, string sville_comptable, DateTime sdateEmbauche_comptable)
               : base(sid_comptable, snom_comptable, sprenom_comptable, slogin_comptable, smdp_comptable, sadresse_comptable, scp_comptable, sville_comptable, sdateEmbauche_comptable)
        {

        }

    }

    public class Ccomptables
    {
        public Dictionary<string, Ccomptable> ocollDicoCptable;
        public Dictionary<string, Ccomptable> ocollDicoCptableByLogin;
        public Ccomptable ocomptableAuthentifie = null;

        public Ccomptables()
        {
            ocollDicoCptable = new Dictionary<string, Ccomptable>();
            ocollDicoCptableByLogin = new Dictionary<string, Ccomptable>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM comptable";
            MySqlDataReader ord = odao.getReader(query);
            
            while (ord.Read())
            {
                Ccomptable ocomptable = new Ccomptable(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]), Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                ocollDicoCptable.Add(Convert.ToString(ord["id"]), ocomptable);
                ocollDicoCptableByLogin.Add(Convert.ToString(ord["login"]), ocomptable);
            }
        }

        public Ccomptable getComptable(string skey)  //la clef reçue est soit le "login" soit "id"
        {
            // Cette methode s'adapte au deux dictionnaires
            int valeur;
            Ccomptable ocomptable;
            bool trouve = false;

            // Chez les comptables (1 seul comptable pour l'instant !!) le troisième caractère de l'id est un entier
            if (int.TryParse(skey.Substring(2, 1), out valeur)) // permet de verifier que le deuxieme caractere de la clef est un int
            {
                trouve = ocollDicoCptable.TryGetValue(skey, out ocomptable);
            }
            else
            {
                trouve = ocollDicoCptableByLogin.TryGetValue(skey, out ocomptable);

            }

            if (trouve)
            {
                return ocomptable;
            }
            else
            {
                return null;
            }

        }

        public DataSet getdsComptable()
        {
            Cdao odao = new Cdao();
            string query = "select * from comptable";
            DataSet ods = odao.getDataSet(query);
            return ods;
        }

        private static Ccomptables Instance = null;
        public static Ccomptables getInstance()
        {
            if (Instance == null)
            {
                Instance = new Ccomptables();
                return Instance;
            }
            else
            {
                return Instance;
            }


        }
    }

    #endregion


    #region Ligne de frais forfait ou HF et fiche de frais : classes métier et contrôle

    // Réaliser un héritage avec CligneFrais 2 cattributs communs entre FHF et FF
    public class CligneFHF
    {
        public string id { get; set; }
        public Cvisiteur ovisiteur { get; set; }
        public string mois { get; set; }
        public string libelle { get; set; }
        public DateTime date { get; set; }
        public double montant { get; set; }



        public CligneFHF(string sid, Cvisiteur sovisiteur)
        {

        }


    }

    public class CligneFHFs
    {
        public CligneFHFs()
        {
            //int x = null;
            //string y = null;
            //while
            //appel a la base de donnees (select visiteur de la ligne de frais HF)

            Cvisiteurs ovisiteurs = Cvisiteurs.getInstance();
            Cvisiteur ovisiteur = ovisiteurs.getVisiteur("id");
            CligneFHF oligne = new CligneFHF("mon id",ovisiteur);
        }

    }
    #endregion


}
