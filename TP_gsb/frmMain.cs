﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_gsb.mesClasses;

namespace TP_gsb
{
    public partial class frmMain : Form
    {

        public frmMain()
        {
            InitializeComponent();
        }

        private void saisirNouveauVisiteurMalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Code à implémenter
            /*Ce sous-menu ne sera accessible qu'au comptable de l'entreprise GSB*/
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listeDesVisiteursDeLaSociétéToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //Code à implémenter
            /*Ce sous-menu ne sera accessible qu'au comptable de l'entreprise GSB*/

        }

        private void btnAuthentification_Click(object sender, EventArgs e)
        {
            // obtient la référence vers la classe 
            Cvisiteurs ovisiteurs = Cvisiteurs.getInstance();
            Ccomptables ocomptables = Ccomptables.getInstance();
            Cemploye oemploye = null; // Polymorphisme d'héritage
            //teste si on rentre dans le premier if
            bool estRentreIci = false;
            Button obtnSender = (Button)sender;

            if (tbMdp.Text != string.Empty && tbLogin.Text != string.Empty && obtnSender.Text == "Connexion")
            {
                    estRentreIci = true;
                    
                    //Récupère si possible la référence de l'objet visiteur correspondant au login 
                    oemploye = ovisiteurs.getVisiteur(tbLogin.Text.Trim());
                    if (oemploye == null)
                    {
                        // Essaye de récupérer un objet comptable si l'objet visiteur n'a pas été récupéré
                        oemploye = ocomptables.getComptable(tbLogin.Text.Trim());
                    }
                    // Si un objet a été récupéré 
                    if (oemploye != null)
                    {
                        //Vérification du mot de passe
                        //La méthode Trim enlève les espaces à gauche et à droite de la chaine
                        if (tbMdp.Text.Trim() == oemploye.mdp_employe.Trim())
                        {
                            CemployeAuthentifie.oemplAuthentifie = oemploye;
                            oemploye.estAuthentifie = true;
                            tbLogin.Text = string.Empty;
                            tbMdp.Text = string.Empty;
                            if (oemploye is Cvisiteur) // Polymorphisme employe-visiteur
                            {
                                MessageBox.Show("Vous êtes connecté en tant que Visiteur");
                            }
                            else
                            {
                                MessageBox.Show("Vous êtes connecté en tant que Comptable");

                            }
                            obtnSender.Text = "Déconnexion";
                        }
                        else
                        {
                            tbLogin.Text = string.Empty;
                            tbMdp.Text = string.Empty;
                            MessageBox.Show("Login invalide !");

                        }

                    }
                    else // Cas où aucun objet de type Cvisiteur n'est renvoyé par la méthode getVisiteur de l'objet de contrôle
                    {
                        tbLogin.Text = string.Empty;
                        tbMdp.Text = string.Empty;
                        MessageBox.Show("Login invalide !");

                    }
              
                
            }
            else
            {
                if (this.btnAuthentification.Text == "Connexion")
                {
                    MessageBox.Show("Vous devez saisir un login !");
                }
            }



            if (this.btnAuthentification.Text == "Déconnexion" && !estRentreIci)
            {
                //je mets l'attribut estAuthentifie de l'employé authentifie à false
                CemployeAuthentifie.oemplAuthentifie.estAuthentifie = false;
                //Dans la classe statique CemployeAuthentifie je met à null la variable objet de type Cemploye puisqu'il se déconnecte
                CemployeAuthentifie.oemplAuthentifie = null; //Cette action ne tue pas l'objet puisqu'il reste référencé dans la collection tenue par la classe de contrôle correspondante
                //J'averti l'utilisateur de sa déconnexion
                MessageBox.Show("Vous êtes déconnecté !");
                //Vidage des TB au cas où
                tbLogin.Text = string.Empty;
                tbMdp.Text = string.Empty;
                // Change le texte du bouton
                this.btnAuthentification.Text = "Connexion";
            }
            


        }

        private void validationDesFichesDeFraisVisiteursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Code à implémenter
            /*Ce sous-menu ne sera accessible qu'au comptable de l'entreprise GSB*/
        }

        private void saisieDesFichesDeFraisMensuellesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Code à implémenter
            /*Ce sous-menu ne sera accessible qu'aux visiteurs commerciaux de l'entreprise GSB*/
        }


    }
}
