﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace corr_SI6
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
           
            InitializeComponent();
            tbAffiche.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;


        }

        private void btnAffiche_Click(object sender, EventArgs e)
        {
           tbAffiche.Clear();
           string connectionString = "SERVER=127.0.0.1; DATABASE=gsb; UID=root; PASSWORD=";
           MySqlConnection oconn = new MySqlConnection(connectionString);
           oconn.Open();
           string requete = "select id,prenom,nom from visiteur";
           MySqlCommand ocmd = new MySqlCommand(requete, oconn);
           MySqlDataReader ord = ocmd.ExecuteReader();
           while(ord.Read())
           {
                //string ligne = ord["id"] + " " + ord["prenom"] + " " + ord["nom"] + "\r\n";
                string ligne = ord[0] + " " + ord[1] + " " + ord[2] + "\r\n";
                tbAffiche.Text = tbAffiche.Text + ligne;

           }
            //ord.Close();
            oconn.Close();
           

        }
    }
}
