﻿namespace corr_SI6
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbAffiche = new System.Windows.Forms.TextBox();
            this.btnAffiche = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbAffiche
            // 
            this.tbAffiche.Location = new System.Drawing.Point(51, 66);
            this.tbAffiche.Multiline = true;
            this.tbAffiche.Name = "tbAffiche";
            this.tbAffiche.Size = new System.Drawing.Size(360, 188);
            this.tbAffiche.TabIndex = 0;
            // 
            // btnAffiche
            // 
            this.btnAffiche.Location = new System.Drawing.Point(271, 272);
            this.btnAffiche.Name = "btnAffiche";
            this.btnAffiche.Size = new System.Drawing.Size(140, 55);
            this.btnAffiche.TabIndex = 1;
            this.btnAffiche.Text = "Valider";
            this.btnAffiche.UseVisualStyleBackColor = true;
            this.btnAffiche.Click += new System.EventHandler(this.btnAffiche_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 394);
            this.Controls.Add(this.btnAffiche);
            this.Controls.Add(this.tbAffiche);
            this.Name = "frmMain";
            this.Text = "Correction exo_acces-données_S6";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAffiche;
        private System.Windows.Forms.Button btnAffiche;
    }
}

